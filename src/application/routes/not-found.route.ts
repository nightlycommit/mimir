import {Middleware} from "@arabesque/core";
import {Context} from "../context";

export const createNotFoundRoute = (): Middleware<Context> => {
    return (context, next) => {
        const {response} = context;

        response.statusCode = 404;

        return next(context);
    };
};