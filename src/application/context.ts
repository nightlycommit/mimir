import {IncomingMessage, ServerResponse} from "http";
import {Context as HttpContext} from "@arabesque/listener-http";

export class Request extends IncomingMessage {
    getBody(): Promise<Buffer> {
        return new Promise((resolve) => {
            let body = Buffer.from('');

            this.on("data", (chunk) => {
                body += chunk;
            });

            this.on("end", () => {
                resolve(body);
            });
        });
    }
}

export class Response extends ServerResponse {
    private _body: Buffer | null = null;

    set body(value: Buffer) {
        this._body = value;
    }

    end(done?: () => void): this {
        return super.end(this._body, done);
    }
}

export type Context = HttpContext<Request, Response>;