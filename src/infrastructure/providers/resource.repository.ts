import {ResourceRepository as ResourceRepositoryInterface} from "../../domain/repositories/resource.repository";
import {readFile, stat} from "fs";
import {join} from "path";
import {outputFile, rm} from "fs-extra";

export class ResourceRepository implements ResourceRepositoryInterface {
    constructor(
        private readonly storagePath: string
    ) {
    }

    get(path: string): Promise<Buffer | null> {
        return new Promise((resolve) => {
            const storagePath = `${join(this.storagePath, path)}`;

            stat(storagePath, () => {
                readFile(`${storagePath}`, (error, data) => {
                    if (error) {
                        resolve(null);
                    } else {
                        resolve(data);
                    }
                });
            });
        });
    }

    set(path: string, data: Buffer) {
        return outputFile(join(this.storagePath, path), data);
    }

    delete(path: string): Promise<void> {
        return rm(join(this.storagePath, path), {
            recursive: true,
            force: true
        });
    }
}