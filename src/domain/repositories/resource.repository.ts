export type ResourceRepository = {
    get: (path: string) => Promise<Buffer | null>;
    set: (path: string, content: Buffer) => Promise<void>;
    delete: (path: string) => Promise<void>;
};