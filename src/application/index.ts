import {createApplication} from "@arabesque/core";
import {createListener} from "@arabesque/listener-http";
import {createServer} from "http";
import {Request, Response} from "./context";
import {ResourceRepository} from "../infrastructure/providers/resource.repository";
import {createRouter} from "./router";

type Configuration = {
    storagePath: string
};

export const create = (
    configuration: Configuration
) => {
    const {storagePath} = configuration;

    const listener = createListener<Request, Response>(createServer({
        IncomingMessage: Request,
        ServerResponse: Response
    }));

    const resourceRepository = new ResourceRepository(storagePath);

    const application = createApplication(listener, (context, next) => {
        const router = createRouter(resourceRepository);

        return router(context, next).catch((error) => {
            context.response.statusCode = 500;
            context.response.statusMessage = error.message;

            return next(context);
        });
    });

    return (port: number) => {
        return application(port)
            .then(() => {
                console.log(`Application started and listening to port ${port}`);
            });
    };
};
