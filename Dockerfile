# builder
FROM node:16 as builder

WORKDIR /usr/local/src/mimir

ADD ./src ./src
ADD ./test ./test
ADD ./package.json ./package.json
ADD ./tsconfig.json ./tsconfig.json

RUN npm install
RUN npm run bundle

# runner
FROM node:16-slim as runner

WORKDIR /usr/local/mimir

COPY --from=builder /usr/local/src/mimir/dist ./

# application
FROM runner as application

ENTRYPOINT ["node", "."]

# default
FROM application

ENTRYPOINT ["sh", "-c", "node . --storage-path=$STORAGE_PATH --port=$PORT"]
