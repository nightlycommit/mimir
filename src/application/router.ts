import type {Middleware} from "@arabesque/core";
import type {Context} from "./context";
import {createORMiddleware} from "@arabesque/logic-middlewares";
import {createGetRoute} from "./routes/get.route";
import {createUploadRoute} from "./routes/upload.route";
import {createDeleteRoute} from "./routes/delete";
import {createNotFoundRoute} from "./routes/not-found.route";
import type {ResourceRepository} from "../domain/repositories/resource.repository";

export const createRouter = (
    documentRepository: ResourceRepository
): Middleware<Context> => {
    return createORMiddleware(
        createGetRoute(documentRepository),
        createUploadRoute(documentRepository),
        createDeleteRoute(documentRepository),
        createNotFoundRoute()
    );
}