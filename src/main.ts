import {create} from "./application";

import {createCommand, createOption} from "commander";

type ProgramOptions = {
    storagePath: string;
    port: number;
};

export default () => {
    const program = createCommand('mimir')
        .addOption(createOption('-s, --storage-path <storagePath>')
            .makeOptionMandatory())
        .addOption(createOption('-p, --port <port>')
            .makeOptionMandatory())
        .action((options: ProgramOptions) => {
            const {storagePath, port} = options;

            const application = create({
                storagePath
            });

            return application(port).then();
        });

    program.parse();
};
