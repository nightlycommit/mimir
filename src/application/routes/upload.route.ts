import type {Middleware} from "@arabesque/core";
import type {Context} from "../context";
import type {ResourceRepository} from "../../domain/repositories/resource.repository";
import {Writable} from "stream";
import * as createBusboy from "busboy";
import {join} from "path";

export const createUploadRoute = (
    resourceRepository: ResourceRepository
): Middleware<Context> => {
    return (context, next) => {
        const {message} = context;
        const {url} = message;

        let parts: RegExpExecArray | null = null;

        const pattern = /upload$|upload\/(.*)$/;

        if (url && message.method === "POST" && (parts = pattern.exec(url))) {
            const namespace = parts[1] || '';

            return new Promise((resolve, reject) => {
                const busboy = createBusboy({
                    headers: message.headers
                });

                busboy.on('file', (_name, stream, info) => {
                    const {filename} = info;

                    const filePath = join(namespace, filename);

                    let data: Buffer = Buffer.from('');

                    const writable = new Writable({
                        write: (chunk, _encoding, done) => {
                            data = Buffer.concat([data, chunk]);

                            done();
                        }
                    });

                    writable.on('finish', () => {
                        resourceRepository.set(filePath, data)
                            .then(() => {
                                console.log(`"${filename}" uploaded to "${filePath}"`);
                            });
                    });

                    stream.pipe(writable);
                });

                busboy.on('error', (error) => {
                    reject(error);
                });

                busboy.on('finish', () => {
                    resolve(next(context));
                });

                message.pipe(busboy);
            });
        } else {
            return Promise.resolve(context);
        }
    };
};