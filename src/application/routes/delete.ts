import type {Middleware} from "@arabesque/core";
import type {Context} from "../context";
import type {ResourceRepository} from "../../domain/repositories/resource.repository";

export const createDeleteRoute = (
    resourceRepository: ResourceRepository
): Middleware<Context> => {
    return (context, next) => {
        const {message} = context;
        const {url} = message;

        const pattern = /^\/(.*)$/;

        let parts: RegExpExecArray | null = null;

        if (url && message.method === "DELETE" && (parts = pattern.exec(url))) {
            const path = parts[1];

            return resourceRepository.delete(path)
                .then(() => {
                    return next(context);
                });
        } else {
            return Promise.resolve(context);
        }
    };
};