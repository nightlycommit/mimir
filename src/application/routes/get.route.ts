import type {Middleware} from "@arabesque/core";
import type {Context} from "../context";
import type {ResourceRepository} from "../../domain/repositories/resource.repository";
import {lookup} from "mime-types";
import {unescape} from "querystring";

export const createGetRoute = (
    resourceRepository: ResourceRepository
): Middleware<Context> => {
    return (context, next) => {
        const {message, response} = context;
        const {url} = message;

        let parts: RegExpExecArray | null = null;

        const pattern = /^\/(.*)$/;

        if (url && message.method === "GET" && (parts = pattern.exec(url))) {
            const path = unescape(parts[1]);

            return resourceRepository.get(path)
                .then((data) => {
                    if (!data) {
                        return Promise.resolve(context);
                    } else {
                        response.setHeader('content-type', lookup(path) || 'application/octet-stream');
                        response.body = data;
                    }

                    return next(context);
                });
        } else {
            return Promise.resolve(context);
        }
    };
};